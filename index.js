function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    if (typeof letter === 'string' && letter.length === 1) {

    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
        for (let i = 0; i < sentence.length; i++) {
            if (sentence[i] === letter) {
            result++;
        }
    }
    return result;

    // If letter is invalid, return undefined.
    } else {
    return undefined;
    }
}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    text = text.toLowerCase();

    // If the function finds a repeating letter, return false. Otherwise, return true.
    for(let i = 0; i < text.length; i++) {
        if(text.indexOf(text[i]) !== text.lastIndexOf(text[i])) {
            return false
        }
    }
    return true;    
}


function purchase(age, price) {
    // Return undefined for people aged below 13.
    if (age < 13) {
        return undefined;

    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    } else if (age >= 13 && age <= 21 || age >= 65) {
        return (price * 0.8).toFixed(2);

    // Return the rounded off price for people aged 22 to 64.
    } else {
        return price.toFixed(2);
    }
    // The returned value should be a string.  
}


function findHotCategories(items) {
    const hotCategories = [];

    // Find categories that has no more stocks.
    items.forEach((item) => {
        if (item.stocks === 0) {
            
        // The hot categories must be unique; no repeating categories.
            if (!hotCategories.includes(item.category)) {
                hotCategories.push(item.category);
            }
        }
    });
    return hotCategories;
}


function findFlyingVoters(candidateA, candidateB) {
    const votersA = new Set(candidateA);
    const votersB = new Set(candidateB);
    const flyingVoters = [];

    // Find voters who voted for both candidate A and candidate B.
    for (const voter of votersA) {
        if (votersB.has(voter)) {
            flyingVoters.push(voter);
        }
    }
    return flyingVoters;
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};